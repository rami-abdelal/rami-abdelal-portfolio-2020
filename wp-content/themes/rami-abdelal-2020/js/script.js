jQuery(function($) {
  (function lazyLoad() {
    var lazyLoadInstance = new LazyLoad({
      elements_selector: ".lazy"
    });
  })();

  (function effects() {
    var breakpoint = 768;
    var rellax;
    var rellaxClass = "supports-rellax";
    function handleEffects() {
      if (!rellax && $(window).width() > breakpoint) {
        rellax = new Rellax(".rellax", { center: true });
        $("body").addClass(rellaxClass);
      } else if (rellax && $(window).width() <= breakpoint) {
        rellax.destroy();
        rellax = null;
        $("body").removeClass(rellaxClass);
      }
    }
    handleEffects();
    $(window).on("resize", function() {
      handleEffects();
    });
  })();

  (function modals() {
    var handle = ".work__item";
    var modal = ".modal";
    var closedClass = "modal--closed";
    var closingClass = "modal--closing";
    var attr = "data-id";
    var idClass = ".modal--";
    var close = ".cross";
    var delay = 200;
    function openModal() {
      $(handle).on("click", function() {
        var selectedModal = idClass + $(this).attr(attr);
        $(selectedModal).removeClass(closedClass);
        $("html, body").css({ overflow: "hidden" });
      });
    }
    function closeModal() {
      $(close).on("click", function() {
        $(modal).addClass(closingClass);
        $("html, body").css({ overflow: "" });
        setTimeout(function() {
          $(modal)
            .removeClass(closingClass)
            .addClass(closedClass);
        }, delay);
      });
    }
    openModal();
    closeModal();
  })();

  function hoverSkills() {
    var parent = ".skills__item";
    var child = ".skill__subskills";
    console.log("script");
    var maxHeight = 0;
    $(child).each(function() {
      var thisHeight = $(this).height();
      if (thisHeight > maxHeight) {
        maxHeight = thisHeight;
      }
      $(this).data("height", thisHeight);
      console.log("data", $(this), thisHeight);
      $(this).css({
        maxHeight: 0,
        overflow: "hidden",
        transition: "max-height .2s ease-in-out"
      });
    });
    $(parent)
      .parent()
      .css({ minHeight: maxHeight });
    $(parent).on("mouseenter", function() {
      $(this)
        .find(child)
        .css({
          maxHeight: $(this)
            .find(child)
            .data("height")
        });
      console.log("enter", $(this).data("height"), $(this).find(child));
    });
    $(parent).on("mouseleave", function() {
      $(this)
        .find(child)
        .css({ maxHeight: 0 });
      console.log("leave", $(this), $(this).find(child));
    });
  }
});
