<?php

/**
 * Hero Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'hero-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hero';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
?>

<section class="hero__container rellax" style="background-image:url(<?= get_field("hero_background_image")["url"] ?>)" data-rellax-speed="-10">
    <div class="container rellax" data-rellax-speed="3">
        <h1 class="hero__title"><?= get_field("hero_title") ?></h1>
        <h2 class="hero__subtitle"><?= get_field("hero_subtitle") ?></h2>
    </div>
</section>