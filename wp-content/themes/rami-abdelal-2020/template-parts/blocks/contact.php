<?php

/**
 * Contact Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'contact-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'contact';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
?>
<h3 class="contact__title hiding-title">Contact</h3>
<div class="slant-wrapper">
    <section class="contact__container slant">
        <div class="container">
            <h6 class="small-title">Get in touch</h6>
            <div class="contact__methods">
                <div class="email">
                    <h5 class="email__title">Email</h5>
                    <a href="mailto:<?= get_field("contact_email") ?>" target="_blank"><?= get_field("contact_email") ?></a>
                </div>
                <div class="social">
                    <h5 class="social__title">Social</h5>
                    <a href="<?= get_field("contact_social_url") ?>" target="_blank"><?= get_field("contact_social_name") ?></a>
                </div>
                <div class="spacer"></div>
                <div class="phone">
                    <h5 class="phone__title">Phone</h5>
                    <a href="tel:<?= get_field("contact_phone") ?>" target="_blank"><?= get_field("contact_phone") ?></a>
                </div>
                <div class="repo">
                    <h5 class="repo__title">Bitbucket</h5>
                    <a href="<?= get_field("contact_repo_url") ?>" target="_blank"><?= get_field("contact_repo_name") ?></a>
                </div>
            </div>
        </div>
    </section>
</div>