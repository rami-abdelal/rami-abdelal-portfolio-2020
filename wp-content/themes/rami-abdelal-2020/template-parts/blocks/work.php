<?php

/**
 * Work Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'work-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'work';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
?>
<h3 class="work__title hiding-title">Work</h3>
<section class="work__container">
    <div class="slant-wrapper">
        <div class="featured-project__container slant">
            <div class="featured-project__image" style="background-image:url(<?= get_field("work_featured_project_image")["url"] ?>)"></div>
            <div class="featured-project__text">
                <h6 class="small-title">Featured project</h6>
                <h4 class="featured-project__title"><?= get_field("work_featured_project_title") ?></h4>
                <h5 class="featured-project__subtitle"><?= get_field("work_featured_project_subtitle") ?></h5>
                <p class="featured-project__description"><?= get_field("work_featured_project_description") ?></p>
                <a class="featured-project__link" href="<?= get_field("work_featured_project_url") ?>">Visit</a>
            </div>
        </div>
    </div>
    <div class="work__items rellax"  data-rellax-speed="-5">
        <div class="container">
            <h6 class="small-title">Recent projects</h6>
            <?php 
                $args = array( 'post_type' => 'portfolio item', 'posts_per_page' => get_field("work_item_count"), 'orderby' => 'menu_order');
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
            ?>
            <a class="work__item" data-id="<?= get_the_ID() ?>" href="javascript:void(0)" data-tilt data-tilt-gyroscope="true" data-tilt-scale="1.05" data-tilt-speed="200" data-tilt-glare="true" data-tilt-max-glare="0.2" data-tilt-perspective="1000">
                <div class="item__image">
                    <img src="<?= get_field("item_image", get_the_ID())["url"] ?>">
                </div>
                <div class="item__text">
                    <h4 class="item__title"><?= get_the_title() ?></h4>
                    <p class="item__subtitle"><?= get_field("item_subtitle", get_the_ID()) ?></p>
                    <p class="item__link">Learn more</p>
                </div>
            </a>
            <?php endwhile; wp_reset_postdata(); ?>
            <?php if (get_field("work_item_count") % 2 !== 0) : ?>
                <div class="work__item"></div>
            <?php endif; ?>
        </div>
    </div>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <div class="slant-wrapper modal modal--<?= get_the_ID() ?> modal--closed">
            <div class="modal__container slant">
                <div class="modal__images scroll" data-simplebar>
                    <?php if ($images = get_field("item_gallery", get_the_ID())) : foreach ($images as $image) : ?>
                        <img class="modal__image lazy" data-src="<?= $image["url"] ?>">
                    <?php endforeach; endif; ?>
                </div>
                <div class="modal__text scroll" data-simplebar>
                    <a class="cross" href="javascript:void(0)"><svg xmlns="http://www.w3.org/2000/svg" id="a83f54af-8247-47dc-a149-791e399f1f9e" data-name="Layer 1" viewBox="0 0 767.31 767.74"><title>cross</title><polygon points="767.31 727.46 727.51 767.26 383.9 423.65 39.8 767.74 0 727.94 344.1 383.85 0.05 39.8 39.85 0 383.9 344.05 727.46 0.48 767.26 40.28 423.7 383.85 767.31 727.46"/></svg></a>
                    <h4 class="modal__title"><?= get_the_title() ?></h4>
                    <h5 class="modal__subtitle"><?= get_field("item_tech", get_the_ID()) ?></h5>
                    <div class="modal__description"><?= get_the_content() ?></div>
                    <a class="modal__link" href="<?= get_field("item_url", get_the_ID()) ?>">Visit</a>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</section>