<?php

/**
 * Skills Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'skills-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'skills';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
?>

<h3 class="skills__title hiding-title">Skills</h3>
<div class="slant-wrapper">
    <section class="skills__container slant" style="background-image:url(<?= get_field("skills_background_image")["url"] ?>)">
        <div class="container">
            <h6 class="small-title">Skills</h6>
            <div class="skills__items">
                <?php 
                    $args = array( 'post_type' => 'skill', 'posts_per_page' => 3, 'orderby' => 'menu_order');
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();
                ?>
                <div class="skills__item" data-tilt data-tilt-gyroscope="true" data-tilt-scale="1.05" data-tilt-speed="200" data-tilt-glare="true" data-tilt-max-glare="0.2" data-tilt-perspective="1500">
                    <h4 class="skill__title"><?= get_the_title() ?></h4>
                    <p class="skill__years"><span class="skill__years-number"><?= get_field("skills_years", get_the_ID()) ?></span><span class="skill__years-suffix">yrs</span></p>
                    <?php $subSkills = get_field("skills_sub_skills", get_the_ID()); ?>    
                    <?php if ($subSkills) :?>
                        <ul class="skill__subskills">
                            <?php foreach ($subSkills as $skill) : ?>
                                <li class="skill__subskill"><?= $skill["name"] ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
                <div class="skills__other">
                    <h5 class="skills__other-title">Other skills</h5>
                    <p class="skills__other-description"><?= get_field("skills_other_skills") ?></p>
                </div>
            </div>
        </div>
    </section>
</div>