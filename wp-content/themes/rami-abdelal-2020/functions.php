<?php
/**
 * Rami Abdelal 2020 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Rami_Abdelal_2020
 */

if ( ! function_exists( 'rami_abdelal_2020_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function rami_abdelal_2020_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Rami Abdelal 2020, use a find and replace
		 * to change 'rami-abdelal-2020' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'rami-abdelal-2020', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'rami-abdelal-2020' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'rami_abdelal_2020_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'rami_abdelal_2020_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function rami_abdelal_2020_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'rami_abdelal_2020_content_width', 640 );
}
add_action( 'after_setup_theme', 'rami_abdelal_2020_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function rami_abdelal_2020_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'rami-abdelal-2020' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'rami-abdelal-2020' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'rami_abdelal_2020_widgets_init' );

function rami_abdelal_2020_fonts() {
	wp_enqueue_style("rami-abdelal-2020-fonts", "https://use.typekit.net/gra4nec.css");
}

/**
 * Enqueue scripts and styles.
 */
function rami_abdelal_2020_scripts() {
	wp_enqueue_style("rami-abdelal-2020-fonts", "https://use.typekit.net/gra4nec.css");
	wp_enqueue_style('rami-abdelal-2020-style', get_stylesheet_uri() );
	wp_enqueue_style("rami-abdelal-2020-overlay-scrollbars", get_template_directory_uri() . "/OverlayScrollbars.min.css");
	wp_enqueue_style("rami-abdelal-2020-simple-bars", "https://unpkg.com/simplebar@latest/dist/simplebar.css");
	wp_enqueue_script('intersection-observer', "https://cdn.jsdelivr.net/npm/intersection-observer@0.7.0/intersection-observer.js", ["jquery"], '20151215', true );
	wp_enqueue_script('lazy-load', "https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.4.0/dist/lazyload.min.js", ["jquery"], '20151215', true );
	wp_enqueue_script('simpleBars', "https://unpkg.com/simplebar@latest/dist/simplebar.min.js", ["jquery"], '20151215', true );
	wp_enqueue_script('vanilla-tilt', get_template_directory_uri() . '/js/vanilla-tilt.min.js', ["jquery"], '20151215', true );
	wp_enqueue_script('rellax', get_template_directory_uri() . '/js/jquery.overlayScrollbars.min.js', ["jquery"], '20151215', true );
	wp_enqueue_script('overlay-scrollbars', get_template_directory_uri() . '/js/rellax.min.js', ["jquery"], '20151215', true );
	//wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/jquery.waypoints.min.js', [], '20151215', true );
	wp_enqueue_script('rami-abdelal-2020-js', get_template_directory_uri() . '/js/script.js', ["jquery"], '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'rami_abdelal_2020_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/** 
 * Load Advanced Custom Fields
 */
require get_template_directory() . '/inc/advanced-custom-fields.php';

/**
 * Register blocks
 */

function ra2020_register_acf_block_types() {

    // Hero
    acf_register_block_type(array(
        'name'              => 'ra2020/hero',
        'title'             => __('Hero'),
        'description'       => __('Hero block for RA2020'),
        'render_template'   => 'template-parts/blocks/hero.php',
        'category'          => 'formatting',
        'icon'              => 'shield',
        'keywords'          => array( 'hero', 'ra2020' ),
	));

	// Work
	acf_register_block_type(array(
		'name'              => 'ra2020/work',
		'title'             => __('Work'),
		'description'       => __('Work block for RA2020'),
		'render_template'   => 'template-parts/blocks/work.php',
		'category'          => 'formatting',
		'icon'              => 'shield',
		'keywords'          => array( 'work', 'ra2020' ),
	));

	// Skills
	acf_register_block_type(array(
		'name'              => 'ra2020/skills',
		'title'             => __('Skills'),
		'description'       => __('Skills block for RA2020'),
		'render_template'   => 'template-parts/blocks/skills.php',
		'category'          => 'formatting',
		'icon'              => 'shield',
		'keywords'          => array( 'skills', 'ra2020' ),
	));

	// Contact
	acf_register_block_type(array(
		'name'              => 'ra2020/contact',
		'title'             => __('Contact'),
		'description'       => __('Contact block for RA2020'),
		'render_template'   => 'template-parts/blocks/contact.php',
		'category'          => 'formatting',
		'icon'              => 'shield',
		'keywords'          => array( 'contact', 'ra2020' ),
	));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'ra2020_register_acf_block_types');
}

/**
 * Register custom post types
 */

function ra2020_register_cpt() {

	$portfolioLabel = array(
		'name'                  => _x( 'Portfolio Items', 'Post type general name', 'rami-abdelal-2020' ),
		'singular_name'         => _x( 'Portfolio Item', 'Post type singular name', 'rami-abdelal-2020' ),
		'menu_name'             => _x( 'Portfolio Items', 'Admin Menu text', 'rami-abdelal-2020' ),
		'name_admin_bar'        => _x( 'Portfolio Item', 'Add New on Toolbar', 'rami-abdelal-2020' ),
		'add_new'               => __( 'Add New', 'rami-abdelal-2020' ),
		'add_new_item'          => __( 'Add New Portfolio Item', 'rami-abdelal-2020' ),
		'new_item'              => __( 'New Portfolio Item', 'rami-abdelal-2020' ),
		'edit_item'             => __( 'Edit Portfolio Item', 'rami-abdelal-2020' ),
		'view_item'             => __( 'View Portfolio Item', 'rami-abdelal-2020' ),
		'all_items'             => __( 'All Portfolio Items', 'rami-abdelal-2020' ),
		'search_items'          => __( 'Search Portfolio Items', 'rami-abdelal-2020' ),
		'parent_item_colon'     => __( 'Parent Portfolio Items:', 'rami-abdelal-2020' ),
		'not_found'             => __( 'No Portfolio Items found.', 'rami-abdelal-2020' ),
		'not_found_in_trash'    => __( 'No Portfolio Items found in Trash.', 'rami-abdelal-2020' ),
		'featured_image'        => _x( 'Portfolio Item Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'archives'              => _x( 'Portfolio Item archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'rami-abdelal-2020' ),
		'insert_into_item'      => _x( 'Insert into Portfolio Item', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'rami-abdelal-2020' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Portfolio Item', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'rami-abdelal-2020' ),
		'filter_items_list'     => _x( 'Filter Portfolio Items list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'rami-abdelal-2020' ),
		'items_list_navigation' => _x( 'Portfolio Items list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'rami-abdelal-2020' ),
		'items_list'            => _x( 'Portfolio Items list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'rami-abdelal-2020' ),
	);

	$portfolioArgs = array(
		'labels'             => $portfolioLabel,
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'portfolio-item' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
	);

	$skillLabel = array(
		'name'                  => _x( 'Skills', 'Post type general name', 'rami-abdelal-2020' ),
		'singular_name'         => _x( 'Skill', 'Post type singular name', 'rami-abdelal-2020' ),
		'menu_name'             => _x( 'Skills', 'Admin Menu text', 'rami-abdelal-2020' ),
		'name_admin_bar'        => _x( 'Skill', 'Add New on Toolbar', 'rami-abdelal-2020' ),
		'add_new'               => __( 'Add New', 'rami-abdelal-2020' ),
		'add_new_item'          => __( 'Add New Skill', 'rami-abdelal-2020' ),
		'new_item'              => __( 'New Skill', 'rami-abdelal-2020' ),
		'edit_item'             => __( 'Edit Skill', 'rami-abdelal-2020' ),
		'view_item'             => __( 'View Skill', 'rami-abdelal-2020' ),
		'all_items'             => __( 'All Skills', 'rami-abdelal-2020' ),
		'search_items'          => __( 'Search Skills', 'rami-abdelal-2020' ),
		'parent_item_colon'     => __( 'Parent Skills:', 'rami-abdelal-2020' ),
		'not_found'             => __( 'No Skills found.', 'rami-abdelal-2020' ),
		'not_found_in_trash'    => __( 'No Skills found in Trash.', 'rami-abdelal-2020' ),
		'featured_image'        => _x( 'Skill Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'rami-abdelal-2020' ),
		'archives'              => _x( 'Skill archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'rami-abdelal-2020' ),
		'insert_into_item'      => _x( 'Insert into Skill', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'rami-abdelal-2020' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Skill', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'rami-abdelal-2020' ),
		'filter_items_list'     => _x( 'Filter Skills list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'rami-abdelal-2020' ),
		'items_list_navigation' => _x( 'Skills list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'rami-abdelal-2020' ),
		'items_list'            => _x( 'Skills list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'rami-abdelal-2020' ),
	);

	$skillArgs = array(
		'labels'             => $skillLabel,
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'skill' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
	);

	register_post_type("portfolio item", $portfolioArgs);
	register_post_type("skill", $skillArgs);
}

add_action("init", "ra2020_register_cpt");